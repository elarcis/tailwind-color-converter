use colorsys::Rgb;
use regex::Regex;

fn main() {
    let color_regex =
        Regex::new(r"(#(?i)([a-f0-9]{2}){3})(ff)?").expect("the color regex should always compile");
    let input = std::fs::read_to_string("in.css")
        .expect("file './in.css' is a hardcoded value and should be existing and readable");
    let output = input
        .lines()
        .map(|line| {
            if let Some(captures) = color_regex.captures(line) {
                let tailwind_rgb = get_tailwind_rgb(&captures[1]);
                line.replace(&captures[0], &tailwind_rgb)
            } else {
                line.to_owned()
            }
        })
        .collect::<Vec<_>>()
        .join("\n");
    std::fs::write("out.css", output).expect("output file './out.css' should be writeable");
}

fn get_tailwind_rgb(hex: &str) -> String {
    let rgb =
        Rgb::from_hex_str(hex).expect("'hex' parameter should be a parseable hexadecimal color");
    format!("{} {} {}", rgb.red(), rgb.green(), rgb.blue())
}
